﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestBitBucketIntegration.Models
{
    public class JsonBitBucket
    {
        public Push push { get; set; }
        public Repository repository { get; set; }
    }
    public class Push
    {
        public List<Change> changes { get; set; }
    }

    public class Change
    {
        public Old old { get; set; }
        public List<Commit> commits { get; set; }
    }

    public class Old
    {
        public string name { get; set; } // branch name
    }

    public class Commit
    {
        public string hash { get; set; } //hash code.
        public Links links { get; set; } // we will get url for push.
        public Author author { get; set; }// we will get user information who have push the code.
        public DateTime date { get; set; }
        public string message { get; set; } // commit message
        public string type { get; set; }//type of action.
    }

    public class Links
    {
        public Html html { get; set; }
    }

    public class Html
    {
        public string href { get; set; }//url to see the push changes.
    }

    public class User
    {
        public string username { get; set; }// username
        public string type { get; set; }//type of user
        public string display_name { get; set; }//display name of user
        public string uuid { get; set; }//Uuid of user

    }

    public class Author
    {
        public string raw { get; set; }//email
        public User user { get; set; }
    }

    public class Owner
    {
        public string username { get; set; } //tpatelbitbucket
        public string type { get; set; }//user
        public string display_name { get; set; }//Tejas Patel
        // public string uuid { get; set; }
    }

    public class Project
    {
        public string key { get; set; }//project key
        public string name { get; set; }//project name
    }

    public class Repository
    {

        public string scm { get; set; }
        public string website { get; set; }// website url
        public string name { get; set; } // project name
        public string type { get; set; }//repository
        public bool is_private { get; set; }//true or false
        public string uuid { get; set; }//repository uuid
        public Project project { get; set; }
        public string full_name { get; set; }
    }
}