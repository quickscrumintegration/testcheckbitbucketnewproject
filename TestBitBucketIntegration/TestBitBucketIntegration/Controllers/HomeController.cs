﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestBitBucketIntegration.Models;

namespace TestBitBucketIntegration.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult BitBucketIntegration()
        {
            return View();
        }

        public ActionResult BitBucketJson(string bitBucketJson)
        {
            try
            {
                JsonBitBucket objBitBucket = JsonConvert.DeserializeObject<JsonBitBucket>(bitBucketJson);
            }
            catch (Exception ex)
            { }
            return Json("Success", JsonRequestBehavior.AllowGet);
        }
    }
}